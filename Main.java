/**
 * This is the main class of the project.
 *
 * @author DUCOBU Alexandre
 */
public class Main {

	static Grid grid;// = new Grid();
	static int level = 1;

	public static void main(String[] args) {
		grid = new Grid(level);
		System.out.print("\033[H\033[2J");
		System.out.println("Level: " + level);
		grid.displayGrid();
		System.out.println("Please write your code:");
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		grid.clear();
		switch (level) {
			case 1:
				moveLevel1();
				break;
			case 2:
				moveLevel2();
				break;
			case 3:
				moveLevel3();
				break;
		}
		System.out.println("\nCup of coffee found !\nLevel completed !");
	}

	public static void moveLevel1() {
		grid.rightMove(1);
		grid.upMove(3); // To much moves to test the canMove function
		grid.leftMove(2);
		grid.getBean();
		grid.rightMove(2);
		grid.move(4, 2);
		grid.rightMove(2);
		grid.upMove(5);
		grid.getBean();
		grid.downMove(6);
		grid.rightMove(2);
		grid.getBean();
		grid.upMove(1);
		grid.rightMove(3);
	}

	public static void moveLevel2() {
		grid.move(1, 0);
		grid.move(2, 2);
		grid.move(4, 0);
		grid.move(1, 2);
		grid.getBean();
		grid.move(2, 3);
		grid.getBean();
		grid.move(1, 2);
		grid.move(3, 0);
	}

	public static void moveLevel3() {
		grid.move(3, 1);
		grid.move(1, 3);
		grid.getBean();
		grid.move(2, 2);
		grid.getBean();
		grid.move(1, 3);
		grid.move(4, 1);
		grid.move(2, 3);
		grid.move(1, 1);
	}

}
