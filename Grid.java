import java.io.*;

/**
 * This class creates a grid object.
 *
 * @author DUCOBU Alexandre
 */
public class Grid {

	private String[][] grid = new String[10][10];
	private FileInputStream fis = null;
	private int ch, row = 0, col = 0, level;
	private int bean = 0, nbBeans = 0;
	private int currentRow, currentCol, arrowRow, arrowCol;
	private String sourceFile, arrow, previousCell;
	private int direction = -1;

	public Grid() {
		sourceFile = "grids";
		level = 0;
		initGrid();
	}

	public Grid(int level) {
		sourceFile = "grids";
		this.level = level;
		initGrid();
	}

	public Grid(String sourceFile, int level) {
		this.sourceFile = sourceFile;
		this.level = level;
		initGrid();
	}

	public void initGrid() {
		try {
			fis = new FileInputStream(new File(sourceFile));
			//Character.forDigit(level, 10); //converts int to char
			while (fis.read() != Character.forDigit(level, 10)) {} //Search the wanted grid
			while ((ch = fis.read()) != -1) {
				if (ch != Character.forDigit(level, 10)) {
					if (row == 10) {
						break;
					} else if (col == 10) { //newline
						row++;
						col = 0;
					} else if (ch != 9 && ch != 10) {
						grid[row][col++] = Character.toString((char) ch);
						if (ch == 'C') {
							currentRow = row;
							currentCol = col-1;
							arrowRow = row;
							arrowCol = col+-1;
						} else if (ch == 'b') {
							nbBeans++;
						}
					}
				}
			}
			//getInitialValues();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// The data feed is closed with finally bloc.
			// So the instructions wil be executed even if there's an exception.
			try {
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void displayGrid() {
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				System.out.print(displayCell(grid[row][col]));
				if (col != 9)
					System.out.print(" ");
				else
					System.out.print("\n");
			}
		}
		System.out.print("\n");
		System.out.println("You have " + bean + " bean(s) on " + nbBeans + ".");
	}

	public String displayCell(String cell) {
		if (cell.equals("N"))
			return "⚀";
		else if (cell.equals("b"))
			return "⦿";
		else if (cell.equals("X"))
			return "✗";
		else if (cell.equals("C")) {
			if (direction == 0)
				return "⊃";
			else if (direction == 1)
				return "⊂";
			else if (direction == 2)
				return "∪";
			return "∩";
		}
		return cell;
	}

	public void getInitialValues() {
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				if (getCell(row, col).equals("C")) {
					currentRow = row;
					currentCol = col;
					arrowRow = row;
					arrowCol = col;
				} else if (getCell(row, col).equals("b")) {
					nbBeans++;
				}
			}
		}
	} //Better to verifies this when init.

	public String getCell(int row, int col) {
		return grid[row][col];
	}

	public void clear() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.print("\033[H\033[2J");
		System.out.println("Level: " + level);
		displayGrid();
	}

	/**
	 * This methods replaces the others 'moveX' methods.
	 * It takes two parameters:<br>
	 *     - nbSteps: the number of steps;<br>
	 *     - direction: the direction (right, left, up or down).
	 * @param nbSteps
	 * 		The number of steps (an integer).
	 * @param direction
	 * 		The direction:<br>
	 * 		 	- 0: right;<br>
	 * 		 	- 1: left;<br>
	 * 		 	- 2: down;<br>
	 * 		 	- 3: up.
	 */
	public void move(int nbSteps, int direction) {
		this.direction = direction;
		for (int i = 0; i < nbSteps; i++) {
			clearArrow();
			arrowRow = currentRow;
			arrowCol = currentCol;
			if (direction == 0 && canMove(currentRow, currentCol + 1)) {
				arrow = "→";
				++currentCol;
			} else if (direction == 1 && canMove(currentRow, currentCol - 1)) {
				arrow = "←";
				--currentCol;
			} else if (direction == 2 && canMove(currentRow + 1, currentCol)) {
				arrow = "↓";
				++currentRow;
			} else if (direction == 3 && canMove(currentRow - 1, currentCol)) {
				arrow = "↑";
				--currentRow;
			} else {
				return;
			}
			grid[arrowRow][arrowCol] = arrow;
			//updateBean(currentRow, currentCol);
			previousCell = grid[currentRow][currentCol];
			grid[currentRow][currentCol] = "C";
			clear();
		}
	} //The user should use leftMove, upMove,… (but in french ?)

	/**
	 * This method moves the 'hero' in the right direction.
	 * @param nbSteps
	 * 		The number of steps the hero makes.
	 */
	public void rightMove(int nbSteps) {
		for (int i = 0; i < nbSteps; i++) {
			if (canMove(currentRow, currentCol+1)) {
				clearArrow();
				arrowRow = currentRow;
				arrowCol = currentCol;
				grid[currentRow][currentCol] = "→";
				//updateBean(currentRow, currentCol+1);
				previousCell = grid[currentRow][currentCol+1];
				grid[currentRow][++currentCol] = "C";
				direction = 0;
			}
			clear();
		}
	}

	/**
	 * This method moves the 'hero' in the left direction.
	 * @param nbSteps
	 * 		The number of steps the hero makes.
	 */
	public void leftMove(int nbSteps) {
		for (int i = 0; i < nbSteps; i++) {
			if (canMove(currentRow, currentCol-1)) {
				clearArrow();
				arrowRow = currentRow;
				arrowCol = currentCol;
				grid[currentRow][currentCol] = "←";
				//updateBean(currentRow, currentCol-1);
				previousCell = grid[currentRow][currentCol-1];
				grid[currentRow][--currentCol] = "C";
				direction = 1;
			}
			clear();
		}
	}

	/**
	 * This method moves the 'hero' in the down direction.
	 * @param nbSteps
	 * 		The number of steps the hero makes.
	 */
	public void downMove(int nbSteps) {
		for (int i = 0; i < nbSteps; i++) {
			if (canMove(currentRow+1, currentCol)) {
				clearArrow();
				arrowRow = currentRow;
				arrowCol = currentCol;
				grid[currentRow][currentCol] = "↓";
				//updateBean(currentRow+1, currentCol);
				previousCell = grid[currentRow+1][currentCol];
				grid[++currentRow][currentCol] = "C";
				direction = 2;
			}
			clear();
		}
	}

	/**
	 * This method moves the 'hero' in the up direction.
	 * @param nbSteps
	 * 		The number of steps the hero makes.
	 */
	public void upMove(int nbSteps) {
		for (int i = 0; i < nbSteps; i++) {
			if (canMove(currentRow-1, currentCol)) {
				clearArrow();
				arrowRow = currentRow;
				arrowCol = currentCol;
				grid[currentRow][currentCol] = "↑";
				//updateBean(currentRow-1, currentCol);
				previousCell = grid[currentRow-1][currentCol];
				grid[--currentRow][currentCol] = "C";
				direction = 3;
			}
			clear();
		}
	}

	public void clearArrow() {
		if (arrowRow == currentRow && arrowCol == currentCol)
			return;
		else if (!grid[arrowRow][arrowCol].equals(" "))
			grid[arrowRow][arrowCol] = " ";
	}

	public boolean getBean() {
		if (previousCell.equals("b")) {
			bean++;
			return true;
		}
		return false;
	}

	public void updateBean(int row, int col) {
		if (grid[row][col].equals("b")) {
			bean++;
		}
	}

	/**
	 * This method returns if the move to the wanted cell is possible.
	 * @param row
	 * 		The row of the wanted cell.
	 * @param col
	 * 		The column of the wanted cell.
	 * @return true if the move is possible, false otherwise.
	 */
	public boolean canMove(int row, int col) {
		if ((row > -1 && row < 10) && (col > -1 && col < 10))
			return !(grid[row][col].equals("D") || grid[row][col].equals("d") ||
					grid[row][col].equals("N"));
		return false;
	}

}
